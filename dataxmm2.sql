-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 10 Jul 2018 pada 13.26
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `dataxmm2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text NOT NULL,
  `merk` text NOT NULL,
  `jumlah` text NOT NULL,
  `harga` text NOT NULL,
  `total` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`no`, `nama`, `merk`, `jumlah`, `harga`, `total`) VALUES
(2, '', 'asd', 'asd', 'asd', 'asd'),
(3, 'as', 'as', 'as', 'as', 'as'),
(4, 'a', 'FjallravenKanken', '1', '100000', '100000'),
(5, '', 'Undear Armour', '90', '100000', '9000000'),
(6, 'qwqwqw', 'Undear Armour', '3', '100000', '300000'),
(8, '', '', '', '', '{{jumlah*}}'),
(27, 'ari', 'Converse', '2', '100.000,00', '200000'),
(28, 'ari', 'Converse', '5', '100.000,00', '500000'),
(29, 'ari', 'Hush Puppies', '7', '100.000,00', '700000'),
(30, 'ari', 'New Balance', '1', '100.000,00', '100000'),
(31, 'ari', 'New Balance', '1', '100.000,00', '100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `conten`
--

CREATE TABLE IF NOT EXISTS `conten` (
  `no` int(4) NOT NULL AUTO_INCREMENT,
  `namalink` varchar(25) NOT NULL,
  `isi` text NOT NULL,
  `file` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data untuk tabel `conten`
--

INSERT INTO `conten` (`no`, `namalink`, `isi`, `file`) VALUES
(1, 'ssad123', '123	142				', 'acer.jpeg'),
(2, 'laut', 'Pemandangan laut sore hari di suatu negara luar213', 'Hydrangeas.jpg'),
(11, 'sda', 'asdasd', 'Tulips.jpg'),
(14, 'rergrlk', 'gdgd', 'Tulips.jpg'),
(22, 'qwe', 'wqe', '2434106-516882_zoro.png'),
(23, 'niplle', 'bfx', '594647e91010448f3ae05197.png'),
(24, 'akshdkljasdkb', 'nbasdkjh', '123.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `iklan`
--

CREATE TABLE IF NOT EXISTS `iklan` (
  `no` int(4) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  `namalink` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `satuan` text NOT NULL,
  `isi` text NOT NULL,
  `file` text NOT NULL,
  `harga` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data untuk tabel `iklan`
--

INSERT INTO `iklan` (`no`, `kategori`, `namalink`, `stok`, `satuan`, `isi`, `file`, `harga`) VALUES
(32, 'topi', 'Undear Armour', 2, '', '', 'underarmour2.jpg', '100000'),
(33, 'sepatu', 'Nike', 21, '', '', 'nike.jpg', '100000'),
(34, 'sepatu', 'Vans', 20, '', '', 'vans.jpg', '100000'),
(35, 'sepatu', 'New Balance', 21, '', '', 'newbalance.jpg', '100000'),
(36, 'sepatu', ' Asics', 0, '', '', 'asics.jpg', '100000'),
(37, 'sepatu', 'Converse', 21, '', '', 'converse.jpg', '100000'),
(38, 'sepatu', 'Reebok', 2, '', '', 'reebok.jpg', '100000'),
(39, 'topi', 'Undear Armour', 2, '', '', 'underarmour1.jpg', '100000'),
(40, 'topi', 'Undear Armour', 2, '', '', 'underarmour.jpg', '100000'),
(41, 'topi', 'Undear Armour', 2, '', '', 'underarmortopi.jpg', '100000'),
(42, 'topi', 'Adidas', 0, '', '', 'adidastopi.jpg', '100000'),
(43, 'topi', 'Cessida', 21, '', '', 'cressidatopi.jpg', '100000'),
(44, 'topi', 'DC', 21, '', '', 'dc1.jpg', '100000'),
(45, 'topi', 'DC', 21, '', '', 'dc2.jpg', '100000'),
(46, 'topi', 'DC', 21, '', '', 'dc3.jpg', '100000'),
(47, 'tas', 'Callitspring', 0, '', '', 'callitspring.jpg', '100000'),
(48, 'tas', 'Famo', 21, '', '', 'famo.jpg', '100000'),
(49, 'tas', 'FjallravenKanken', 21, '', '', 'fjallravenkanken.jpg', '100000'),
(50, 'tas', 'Hush Puppies', 21, '', '', 'hushpuppies.jpg', '100000'),
(51, 'tas', 'Phillipe Jourdan', 21, '', '', 'phillipejourdan.jpg', '100000'),
(52, 'tas', 'Super Dry', 2, '', '', 'superdry.jpg', '100000'),
(53, 'tas', 'Superdry', 2, '', '', 'superdry1.jpg', '100000'),
(54, 'sepatu', 'Adidas', 0, '', '', 'adidas.jpg', '100000'),
(55, 'sepatu', 'Macbeth', 21, '', '', 'macbeth.jpg', '100000'),
(57, 'pakaian', 'Hollister', 21, '', '', 'hollister1.jpg', '100000'),
(58, 'pakaian', 'Hollister', 21, '', '', 'hollister2.jpg', '100000'),
(59, 'pakaian', 'Hollister', 21, 'Pcs', '', 'hollister3.jpg', '100000'),
(60, 'pakaian', 'Hollister', 21, 'Pcs', '', 'hollister4.jpg', '100000'),
(61, 'pakaian', 'Jack Jones', 21, '', '', 'jackjones.jpg', '100000'),
(62, 'pakaian', 'Salt N Pepper', 2, '', '', 'saltnpepper.jpg', '100000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `no` int(4) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`no`, `kategori`) VALUES
(1, 'sepatu'),
(2, 'topi'),
(3, 'tas'),
(4, 'pakaian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `no` int(4) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`no`, `name`, `user`, `password`, `status`) VALUES
(1, 'admin', 'admin', '1234', 0),
(2, 'ari', 'member', 'member', 1),
(4, 'ari', '1234', '1234', 1),
(5, 'kenceng', '123', '123', 1),
(6, 'ceng', 'cengceng', '1234', 1),
(7, 'joni', 'orangngalen', '1234', 1),
(8, 'jani', 'orang', 'orang', 1),
(9, '1', '1', '1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekomen`
--

CREATE TABLE IF NOT EXISTS `rekomen` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  `namalink` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `file` text NOT NULL,
  `harga` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `rekomen`
--

INSERT INTO `rekomen` (`no`, `kategori`, `namalink`, `isi`, `file`, `harga`) VALUES
(1, 'topi', 'Undear Armour', '', 'underarmour2.jpg', ''),
(2, 'tas', 'Super Dry', '', 'superdry.jpg', ''),
(3, 'pakaian', 'Salt N Pepper 	', '', 'saltnpepper.jpg	', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `testi`
--

CREATE TABLE IF NOT EXISTS `testi` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `namalink` text NOT NULL,
  `isi` text NOT NULL,
  `file` text NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `testi`
--

INSERT INTO `testi` (`no`, `namalink`, `isi`, `file`) VALUES
(2, 'ari', 'sepatunya bagus banget melebihi ekspetasi saya', 'img-2.png'),
(3, 'ferdiansyah', 'barang ane udah sampe tujuan, mantap gan semoga makin sukses', 'img-4.png');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
