<?php include "valid_session.php";?>
<!doctype html>
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>SIBe.com Admin</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />

		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />

		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />

		<!-- CORE CSS -->
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- THEME CSS -->
		<link href="assets/css/essentials.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/layout.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />

	</head>
	<!--
		.boxed = boxed version
	-->
	<body>


		<!-- WRAPPER -->
		<div id="wrapper">

			<!-- 
				ASIDE 
				Keep it outside of #wrapper (responsive purpose)
			-->
			<aside id="aside">
				<!--
					Always open:
					<li class="active alays-open">

					LABELS:
						<span class="label label-danger pull-right">1</span>
						<span class="label label-default pull-right">1</span>
						<span class="label label-warning pull-right">1</span>
						<span class="label label-success pull-right">1</span>
						<span class="label label-info pull-right">1</span>
				-->
<nav id="sideNav"><!-- MAIN MENU -->
					<ul class="nav nav-list">
						<li class="active"><!-- dashboard -->
							<a class="dashboard" href="index.php"><!-- warning - url used by default by ajax (if eneabled) -->
								<i class="main-icon fa fa-dashboard"></i> <span>Dashboard</span>
							</a>
						</li>
						
						<li>
							<a href="#">
								<i class="fa fa-menu-arrow pull-right"></i>
								<i class="main-icon fa fa-table"></i> <span>Tables</span>
							</a>
							<ul><!-- submenus -->
								
								<li>
									<a href="#">
										<i class="fa fa-menu-arrow pull-right"></i>
										Datatables
									</a>
									<ul>
										<li><a href="tables.php">Managed Datatables</a></li>
										
									</ul>
									<ul>
										<li><a href="user.php">User Datatables</a></li>
										
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-menu-arrow pull-right"></i>
								<i class="main-icon fa fa-pencil-square-o"></i> <span>Forms</span>
							</a>
							<ul><!-- submenus -->
							
								<li><a href="validation.php">Validation Form</a></li>
								<li><a href="validation_user.php">Validation User</a></li>
								<li><a href="isi.php">Isi Form</a></li>
								<li><a href="isi_user.php">Isi User</a></li>
							</ul>
						</li>
					</ul>

					<!-- SECOND MAIN LIST -->
					<h3>MORE</h3>
					<ul class="nav nav-list">

						<li>
							<a href="../../index.php">
								<i class="main-icon fa fa-link"></i>
								<span class="label label-danger pull-right">PRO</span> <span>Frontend</span>
							</a>
						</li>
					</ul>

				</nav>

				<span id="asidebg"><!-- aside fixed background --></span>
			</aside>
			<!-- /ASIDE -->


			<!-- HEADER -->
			<header id="header">

				<!-- Mobile Button -->
				<button id="mobileMenuBtn"></button>

				<!-- Logo -->
				<span class="logo pull-left">
					<img src="assets/images/sibe.png" alt="admin panel" height="35" />
				</span>

				<form method="get" action="page-search.html" class="search pull-left hidden-xs">
					<input type="text" class="form-control" name="k" placeholder="Search for something..." />
				</form>

				<nav>

					<!-- OPTIONS LIST -->
					<ul class="nav pull-right">

						<!-- USER OPTIONS -->
						<li class="dropdown pull-left">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<img class="user-avatar" alt="" src="assets/images/noavatar.jpg" height="34" /> 
								<span class="user-name">
									<span class="hidden-xs">
										<?php echo $_SESSION['admin'];?> <i class="fa fa-angle-down"></i>
									</span>
								</span>
							</a>
							<ul class="dropdown-menu hold-on-click">
								<li><!-- my calendar -->
									<a href="calendar.html"><i class="fa fa-calendar"></i> Calendar</a>
								</li>
								<li><!-- my inbox -->
									<a href="#"><i class="fa fa-envelope"></i> Inbox &nbsp;
										<span class="label label-default">0</span>
									</a>
								</li>
								<li><!-- settings -->
									<a href="page-user-profile.html"><i class="fa fa-cogs"></i> Settings</a>
								</li>

								<li class="divider"></li>

								<li><!-- lockscreen -->
									<a href="page-lock.html"><i class="fa fa-lock"></i> Lock Screen</a>
								</li>
								<li><!-- logout -->
									<a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a>
								</li>
							</ul>
						</li>
						<!-- /USER OPTIONS -->

					</ul>
					<!-- /OPTIONS LIST -->

				</nav>

			</header>
			<!-- /HEADER -->


			<!-- 
				MIDDLE 
			-->
			<section id="middle">


				<!-- page title -->
				<header id="page-header">
					<h1>Form Validate</h1>
					<ol class="breadcrumb">
						<li><a href="#">Forms</a></li>
						<li class="active">Form Validate</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="row">

						<div class="col-md-6">
						
							<?php include"tampiledit_user.php";?>
						</div>

						<div class="col-md-6">

							<div class="panel panel-default">
								<div class="panel-body">

									<h4>How it's working?</h4>
									<p><em>This is a vrey unique feature because you don't need PHP programming if you want to send the form directly to email.</em></p>
									<hr />
									<p>
										This form is using a validation plugin (automaticaly loaded by assets/js/app.js and a php to send files (php/contact.php)
										You can add how many fields you want without changeing anything on Javascript or PHP
									</p>

								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-body">

									<a href="javascript:;" onclick="jQuery('#pre-0').slideToggle();" class="btn btn-info btn-xs">Show Code</a>
<pre id="pre-0" class="text-left noradius text-danger softhide margin-top-20 margin-bottom-0">
<span class="text-success">&lt;!-- 
	<b class="text-warning">.validate</b> 				- very important, to activate validation plugin

	data-success="Sent! Thank you!" 	- used by toastr to print the message
	data-toastr-position="top-right"	- toastr message position:
		.top-right
		.top-left
		.bottom-right
		.bottom-left


	<span class="text-info bold">NOTE: Add .required class for required fields.</span>
	This form example already used on Careers page: <a href="page-careers.html">page-careers.html</a>
 --&gt;</span>
&lt;form class="<b class="text-warning">validate</b>" action="php/contact.php" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!" data-toastr-position="top-right"&gt;
	&lt;fieldset&gt;
		<span class="text-success">&lt;!-- required [php action request] --&gt;</span>
		&lt;input type="hidden" name="action" value="contact_send" /&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-6 col-sm-6"&gt;
					&lt;label&gt;First Name *&lt;/label&gt;
					&lt;input type="text" name="contact[first_name]" value="" class="form-control <b>required</b>"&gt;
				&lt;/div&gt;
				&lt;div class="col-md-6 col-sm-6"&gt;
					&lt;label&gt;Last Name *&lt;/label&gt;
					&lt;input type="text" name="contact[last_name]" value="" class="form-control <b>required</b>"&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-6 col-sm-6"&gt;
					&lt;label&gt;Email *&lt;/label&gt;
					&lt;input type="email" name="contact[email]" value="" class="form-control <b>required</b>"&gt;
				&lt;/div&gt;
				&lt;div class="col-md-6 col-sm-6"&gt;
					&lt;label&gt;Phone *&lt;/label&gt;
					&lt;input type="text" name="contact[phone]" value="" class="form-control <b>required</b>"&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-12 col-sm-12"&gt;
					&lt;label&gt;Position *&lt;/label&gt;
					&lt;select name="contact[position]" class="form-control pointer <b>required</b>"&gt;
						&lt;option value=""&gt;--- Select ---&lt;/option&gt;
						&lt;option value="Marketing"&gt;PR &amp; Marketing&lt;/option&gt;
						&lt;option value="Developer"&gt;Web Developer&lt;/option&gt;
						&lt;option value="php"&gt;PHP Programmer&lt;/option&gt;
						&lt;option value="Javascript"&gt;Javascript Programmer&lt;/option&gt;
					&lt;/select&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-6 col-sm-6"&gt;
					&lt;label&gt;Expected Salary *&lt;/label&gt;
					&lt;input type="text" name="contact[expected_salary]" value="" class="form-control <b>required</b>"&gt;
				&lt;/div&gt;
				&lt;div class="col-md-6 col-sm-6"&gt;
					&lt;label&gt;Start Date *&lt;/label&gt;
					&lt;input type="text" name="contact[start_date]" value="" class="form-control datepicker <b>required</b>" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false"&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-12 col-sm-12"&gt;
					&lt;label&gt;Experience *&lt;/label&gt;
					&lt;textarea name="contact[experience]" rows="4" class="form-control <b>required</b>"&gt;&lt;/textarea&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-12 col-sm-12"&gt;
					&lt;label&gt;
						Website
						&lt;small class="text-muted"&gt;- optional&lt;/small&gt;
					&lt;/label&gt;
					&lt;input type="text" name="contact[website]" placeholder="http://" class="form-control"&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

		&lt;div class="row"&gt;
			&lt;div class="form-group"&gt;
				&lt;div class="col-md-12"&gt;
					&lt;label&gt;
						File Attachment 
						&lt;small class="text-muted"&gt;Curriculum Vitae - optional&lt;/small&gt;
					&lt;/label&gt;

					&lt;!-- custom file upload --&gt;
					&lt;input class="custom-file-upload" name="contact[attachment]" type="file" id="file" data-btn-text="Select a File" /&gt;
					&lt;small class="text-muted block"&gt;Max file size: 10Mb (zip/pdf/jpg/png)&lt;/small&gt;

				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;

	&lt;/fieldset&gt;

	&lt;div class="row"&gt;
		&lt;div class="col-md-12"&gt;
			&lt;button type="submit" class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30"&gt;
				SEND APPLICATION
				&lt;span class="block font-lato"&gt;We'll get back to you within 48 hours&lt;/span&gt;
			&lt;/button&gt;
		&lt;/div&gt;
	&lt;/div&gt;

&lt;/form&gt;
</pre>
								</div>
							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	</body>
</html>