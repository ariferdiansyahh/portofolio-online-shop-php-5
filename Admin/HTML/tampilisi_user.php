							<div class="panel panel-default">
								<div class="panel-heading panel-heading-transparent">
									<strong>FORM VALIDATION</strong>
								</div>

								<div class="panel-body">

										<form class="validate" action="simpan_user.php" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!" data-toastr-position="top-right">
									
											<fieldset>
												<!-- required [php action request] -->
												<input type="hidden" name="action" value="contact_send" />

												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>NAME</label>
															<input type="text" name="name" class="form-control required">
														</div>

													</div>
												</div>


												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>USER</label>
															<input type="text" name="user" class="form-control required">
														</div>

													</div>
												</div>


												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>PASSWORD</label>
															<input type="password" name="password" class="form-control required">
														</div>

													</div>
												</div>


												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>STATUS</label>
															<input type="text" name="status" class="form-control required">
														</div>

													</div>
												</div>






											</fieldset>

											<div class="row">
												<div class="col-md-12">
													<button type="submit" class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30">
														SEND APPLICATION
														<span class="block font-lato">We'll get back to you within 48 hours</span>
													</button>
												</div>
											</div>

										</form>

								</div>

							</div>
							<!-- /----- -->
