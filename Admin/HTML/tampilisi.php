							<div class="panel panel-default">
								<div class="panel-heading panel-heading-transparent">
									<strong>FORM VALIDATION</strong>
								</div>

								<div class="panel-body">

										<form class="validate" action="simpan_iklan.php" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!" data-toastr-position="top-right">
									
											<fieldset>
												<!-- required [php action request] -->
												<input type="hidden" name="action" value="contact_send" />

												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>Namalink</label>
															<input type="text" name="namalink" class="form-control required">
														</div>

													</div>
												</div>



												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>Kategori</label>
															<select name="kategori" class="form-control pointer required">

																
																<option value="sepatu">SEPATU</option>
																<option value="topi">TOPI</option>
																<option value="tas">TAS</option>
																<option value="pakaian">PAKAIAN</option>
															</select>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>Stock</label>
															<input type="text" name="stok" class="form-control required">
														</div>

													</div>
												</div>

												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>Satuan</label>
															<input type="text" name="satuan" class="form-control required">
														</div>

													</div>
												</div>
												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>Isi</label>
															<textarea name="isi" rows="4" class="form-control" ></textarea>
														</div>
													</div>
												</div>

												<div class="row">
													<div class="form-group">
														<div class="col-md-12 col-sm-12">
															<label>
																Harga
															</label>
															<input type="text" name="harga" placeholder="Rp." class="form-control" >
														</div>
													</div>
												</div>

												<div class="row">
													<div class="form-group">
														<div class="col-md-12">
															<label>
																File  
															</label>

															<!-- custom file upload -->
															<div class="fancy-file-upload fancy-file-primary">
																<i class="fa fa-upload"></i>
																<input type="file" class="form-control" name="file" onchange="jQuery(this).next('input').val(this.value);" />
																<input type="text" class="form-control" placeholder="no file selected" readonly="" />
																<span class="button">Pilih File</span>
															</div>
															<small class="text-muted block">Max file size: 10Mb (zip/pdf/jpg/png)</small>

														</div>
													</div>
												</div>

											</fieldset>

											<div class="row">
												<div class="col-md-12">
													<button type="submit" class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30">
														SEND APPLICATION
														<span class="block font-lato">We'll get back to you within 48 hours</span>
													</button>
												</div>
											</div>

										</form>

								</div>

							</div>
							<!-- /----- -->
