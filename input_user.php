<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="animate.css">
  <script src="plugins/jquery/jquery-2.1.4.min.js"></script>
  <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="css/angular.min.js"></script>
</head>
<body style="background-color:#F4F4F4;">

<div class="container">
  <h1>Form input</h1>
  <form action="simpan_input.php" method="post">
  <div ng-app="">
    <div class="form-group">
      <label style="font-size:20px" for="name">Nama</label>
      <input type="text" class="form-control" id="name" name="name" ng-model="nama" placeholder="nama...">
    </div>

    
    <div class="form-group">
      <label style="font-size:20px" for="user">User</label>
      <input type="text" class="form-control" id="user" name="user" ng-model="user" placeholder="user...">
    </div>

    <div class="form-group">
      <label style="font-size:20px" for="pass">Password</label>
      <input type="password" class="form-control" id="pass" name="pass" ng-model="password" placeholder="password...">
    </div>



    <button type="submit" class="btn btn-default" style="border-radius: 10px; padding-top: 10px; padding-bottom: 10px; padding-left: 20px; padding-right: 20px;">Submit</button>
  </div>
  </form>
</div>

</body>
</html>
    