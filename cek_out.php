<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="animate.css">
  
  <script src="plugins/jquery/jquery-2.1.4.min.js"></script>
  <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
</head>
<style type="text/css">


ul.nav li.dropdown:hover ul.dropdown-menu{
display: block;
}
.glyphicon-chevron-right {
  color: #5FACAC;

}
.glyphicon-chevron-left {
  color: #5FACAC;

}

nav ul li:hover{
 background:#EBEBEB;
}



</style>
<body>
<!--HEADER-->
<nav class="navbar navbar-default" style="background-color: #FFFFFF; padding-top: 10px; padding-bottom: 10px; ">
	<div class="container-fluid">
   <div class="navbar-header"><a class="navbar-header" href="index.php"><img src="images/sibe.png" width="90" height="50" ></a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      
    </div>
<div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php include"hasil1.php";?>
          </ul>
        </li>
        <li><a href="keranjang.php">Keranjang</a></li>
        <li><a href="#">Page 3</a></li>
      </ul>
     <form class="navbar-form navbar-left" action="pencarian.php" method="get">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Search" name="cari">
      <div class="input-group-btn">
        <button class="btn btn-default" type="submit">
        <i class="glyphicon glyphicon-search"></i>
        </button>
      </div>
    </div>
  </form> 
      <ul class="nav navbar-nav navbar-right">
        <li><a href="input_user.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>

        
        <?php include"validasi.php";?>
      </ul>
    </div>
      
    </div>

</nav>


 
<div class="container"  style="margin: none;">
  <h3 >Form CekOut</h3>
  

 <!-- start: Table -->


                 <div class="hero-unit"><p><h4>Harap isi form dibawah ini dengan lengkap dan benar sesuai idenditas anda!</h4></p></div>
                <div class="hero-unit"><p><h4>Total Belanja Anda Rp. <?php echo abs((int)$_GET['total']); ?>,-</h4></p></div> 
    <form id="formku" action="selesai.php" method="post">
    <table class="table table-condensed">
    <input type="hidden" name="total" value="<?php echo abs((int)$_GET['total']); ?>">
    <tr>
        <td><label for="nm_usr">Nama</label></td>
        <td><input name="nm_usr" type="text" class="required" minlength="6" id="nm_usr"  /></td>
      </tr>
      <tr>
        <td><label for="log_usr">Username</label></td>
        <td><input name="log_usr" type="text" class="required" minlength="6" id="log_usr" /></td>
      </tr>
      <tr>
        <td><label for="pas_usr">Password</label></td>
        <td><input name="pas_usr" type="password" class="required" minlength="6" id="pas_usr" /></td>
      </tr>
      <tr>
        <td><label for="email_usr">Email</label></td>
        <td><input name="email_usr" type="text" class="email required" id="email_usr" /></td>
      </tr>
      <tr>
        <td><label for="almt_usr">Alamat</label></td>
        <td><input name="almt_usr" type="text" class="required" id="almt_usr" /></td>
      </tr>
      <tr>
        <td><label for="kp_usr">Kode Pos</label></td>
        <td><input name="kp_usr" type="text" class="required number" minlength="5" maxlength="5" id="kp_usr" /></td>
      </tr>
      <tr>
        <td><label for="kota_usr">Kota</label></td>
        <td><input name="kota_usr" type="text" class="required" minlength="6" id="kota_usr" /></td>
      </tr>
      <tr>
        <td><label for="tlp">No telepon</label></td>
        <td><input name="tlp" type="text" class="required number" minlength="12" id="tlp" /></td>
      </tr>
      <tr>
        <td><label for="rek">No Rekening</label></td>
        <td><input name="rek" type="text" class="required number" minlength="12" id="rek" /></td>
      </tr>
      <tr>
        <td><label for="nmrek">Nama Rekening</label></td>
        <td><input name="nmrek" type="text" class="required" minlength="6" id="nmrek" /></td>
      </tr>
      <tr>
        <td><label for="bank">Bank</label></td>
        <td><select name="bank" class="required">
        <option></option>
        <option value="Mandiri">Mandiri</option>
        <option value="BNI">BNI</option>
        <option value="CIMB">CIMB</option>
        <option value="BCA">BCA</option>
        <option value="Bank Jabar">Bank Jabar</option>
        <option value="Danamon">Danamon</option>
        <option value="BRI">BRI</option>
        <option value="Permata">Permata</option>
        </select>
        </td>
      </tr>
      <tr>
      <td></td>
        <td><input type="submit" value="Simpan Data" name="finish"  class="btn btn-sm btn-primary"/>&nbsp;<a href="index.php" class="btn btn-sm btn-primary">Kembali</a></td>
        </tr>
    </table>
    </form>

        
      <!-- end: Table -->
</div>


<div class="container-fluid" align="center" style="background-color: #CDECE9; padding-top: 15px; padding-bottom: 15px;" >
<h4 style="color: #1B98E0;"> Want to learn more about how we help our clients &nbsp;</h4>
<button type="button" class="btn btn-default" style="border-color: #97D3E6; color:#1B98E0;" >LEARN MORE</button>
</div>



  <div class="container-fluid">
          <div class="row">
             <div class="col-sm-12" style="background-color:#595757; color:white; text-align: center;"><h4> SIBe.com</h4>
             
             <a href="#"><img class="image" border="0" title="#" src="images/fb.png" width="40px" height="40px" ></a>

             <a href="#"><img class="image" border="0"title="#" src="images/ig.png" width="40px" height="40px"></a>

             

             <a href="#"><img class="image" border="0"title="#" src="images/tw.png" width="40px" height="40px"></a>

             
      <br>
      <p>Official </p>
     
    
      Copyright &copy; 2018, Design by</p>
    <p>Ari Ferdiansyah</p>
    </div>
  </div>
</div>


 
</body>
</html>