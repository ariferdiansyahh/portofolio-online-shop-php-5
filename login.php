<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">

  <script src="plugins/jquery/jquery-2.1.4.min.js"></script>
  <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #F4F4F4;">

<div class="container">
  <h2>SIGN IN </h2>
  <form action="login_proses.php" method="post">
    <div class="form-group">
      <label for="email">Email & User</label>
      <input type="text" class="form-control" id="email" placeholder="Enter email" name="name">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="remember"> Remember me</label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

</body>
</html>
